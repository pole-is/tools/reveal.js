import { dest as _dest, src as _src, parallel, series, watch } from "gulp";
import csso from "gulp-csso";
import del from "del";
import doZip from "gulp-zip";
import imagemin from "gulp-imagemin";
import rollup from "./lib/rollup";
import sass from "gulp-sass";
import sass_compiler from "node-sass";
import sourcemaps from "gulp-sourcemaps";
import terser from "gulp-terser";

sass.compiler = sass_compiler;

const SRC = __dirname + "/src";
const DEST = __dirname + "/public";
const UPSTREAM = "node_modules/reveal.js";

const src = (globs, opts = {}) => _src(globs, { base: SRC, ...opts });
const upstream = (globs, opts = {}) => _src(globs, { base: UPSTREAM, ...opts });
const dest = (target, opts = {}) => _dest(target, { base: DEST, ...opts });

export const clean = () => del(`${DEST}/**`, { force: true });

export const misc = () =>
  src([`${SRC}/**/*.{eot,otf,ttf,woff,woff2,json,html}`, `${SRC}/**/LICENSE`])
    .pipe(
      upstream([
        `${UPSTREAM}/**/*.{eot,otf,ttf,woff,woff2,json,html}`,
        `!${UPSTREAM}/test/**`,
        `!${UPSTREAM}/*`,
        `${UPSTREAM}/**/LICENSE`,
      ])
    )
    .pipe(dest(DEST));

export const stylesheets = () =>
  src(`${SRC}/**/*.css`)
    .pipe(
      upstream([
        `${UPSTREAM}/**/*.css`,
        `!${UPSTREAM}/css/theme/**`,
        `!${UPSTREAM}/test/**`,
      ])
    )
    .pipe(sourcemaps.init())
    .pipe(csso())
    .pipe(sourcemaps.write("."))
    .pipe(dest(DEST));

export const images = () =>
  src(`${SRC}/**/*.{png,svg,gif,ico,jpg,jpeg}`)
    .pipe(
      upstream([
        `${UPSTREAM}/**/*.{png,svg,gif,ico,jpg,jpeg}`,
        `!${UPSTREAM}/test/**`,
      ])
    )
    .pipe(imagemin())
    .pipe(dest(DEST));

export const themes = () =>
  src(`${SRC}/css/theme/source/**/*.scss`, { base: `${SRC}/css/theme/source` })
    .pipe(
      upstream(`${UPSTREAM}/css/theme/source/**/*.scss`, {
        base: `${UPSTREAM}/css/theme/source`,
      })
    )
    .pipe(sourcemaps.init())
    .pipe(
      sass({
        includePaths: [`${UPSTREAM}/css/theme/source`],
        outputStyle: "compressed",
      })
    )
    .pipe(sourcemaps.write("."))
    .pipe(dest(`${DEST}/css/theme`));

export const highlightjs_themes = () =>
  src("node_modules/highlight.js/scss/default.scss", {
    base: "node_modules/highlight.js/scss",
  })
    .pipe(sourcemaps.init())
    .pipe(
      sass({
        outputStyle: "compressed",
      })
    )
    .pipe(sourcemaps.write("."))
    .pipe(dest(`${DEST}/lib/css`));

export const code = () =>
  src([`${SRC}/**/*.js`, `!${SRC}/plugin/**/*.js`])
    .pipe(
      upstream([
        `${UPSTREAM}/**/*.js`,
        `!${UPSTREAM}/test/**`,
        `!${UPSTREAM}/grunt*`,
      ])
    )
    .pipe(sourcemaps.init())
    .pipe(terser())
    .pipe(sourcemaps.write("."))
    .pipe(dest(DEST));

const plugin_code = () =>
  src(`${SRC}/plugin/*/index.js`)
    .pipe(sourcemaps.init())
    .pipe(rollup({}, { globals: { "reveal.js": "Reveal" } }))
    .pipe(terser())
    .pipe(sourcemaps.write("."))
    .pipe(dest(DEST));

const plugin_stylesheets = () =>
  src(`${SRC}/plugin/*/*.{css,scss}`)
    .pipe(sourcemaps.init())
    .pipe(
      sass({
        outputStyle: "compressed",
      })
    )
    .pipe(sourcemaps.write("."))
    .pipe(dest(DEST));

export const plugins = parallel(plugin_code, plugin_stylesheets);

export const build = series(
  clean,
  parallel(misc, images, code, plugins, stylesheets, themes, highlightjs_themes)
);

const archive = () =>
  src([`${DEST}/**`, `${DEST}/**/*.zip`], { base: DEST })
    .pipe(doZip("reveal-js.zip"))
    .pipe(dest(DEST));

export const zip = series(build, archive);

const watch_themes = () => watch(`${SRC}/css/theme/source/**/*.scss`, themes);
const watch_plugins = () => watch(`${SRC}/plugin/**`, plugins);

export const dev = parallel(watch_themes, watch_plugins);

export default build;
