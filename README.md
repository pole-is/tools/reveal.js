# Introduction

## reveal.js

[reveal.js](https://github.com/hakimel/reveal.js) est un framework pour
construire des présentations HTML.

- Un navigateur est suffisant pour l'afficher.
- Contenu écrit en HTML et/ou en [Markdown](https://fr.wikipedia.org/wiki/Markdown)
- Ce n'est pas WYSIWIG.

## [pole-is/tools/reveal.js](https://gitlab.irstea.fr/pole-is/tools/reveal.js)

Fournit :

- une version de reveal.js intégrant un thème INRAE (circa 2020),
